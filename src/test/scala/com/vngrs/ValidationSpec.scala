package com.vngrs.test

import com.vngrs.User
import org.scalatest.WordSpecLike
import com.vngrs.Validation
import com.vngrs._

class ValidationSpec extends  Validation with WordSpecLike {

  val validUser = User("testUser", "123456", "test@example.com", "Id", "itype")
  val emailProblem = User("testUser", "123456", "example.com", "Id", "itype")
  val passwordProblem = User("testUser", "1234", "test@example.com", "Id", "itype")
  val longString =(1 to 256).map(_ => "a").mkString("","","")
  val tooLongUserName = User(longString, "123456", "test@example.com", "Id", "itype")
  "Validation" should {
    "validate valid users correctly" in {
      assert(validate(validUser) === Valid)
    }
    "not validate if email is not a valid mail" in {
      assert(validate(emailProblem) === InValid(List(MailValidationFailed)) )
    }
    "not validate if password  is less then 6 chars" in {
      assert(validate(passwordProblem) === InValid(List(PassWordTooShort)) )
    }
    "not validate if userName is more thn 255 chars" in {
      assert(validate(tooLongUserName) === InValid(List(TooLongField)))
    }    
  }

  "Validation for multiple checks" should {
    val fullInvaliUser = User(longString, "12", "example.com", "Id", "itype")
    "not validate if more thn one field is invalid" in {
      assert(validate(fullInvaliUser) === InValid(List(PassWordTooShort, TooLongField, MailValidationFailed)))
    }

  }
}
