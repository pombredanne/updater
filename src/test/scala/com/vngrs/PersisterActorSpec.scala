package com.vngrs.test

import akka.actor.ActorSystem
import akka.actor.PoisonPill
import akka.actor.Props
import com.vngrs.DefaultConfig
import com.vngrs.PersisterActor
import akka.testkit.{ TestActorRef, TestKit }
import com.vngrs.User
import java.io.File
import java.io.FileWriter
import java.io.PrintWriter
import org.scalatest.BeforeAndAfterAll
import org.scalatest.Sequential
import org.scalatest.Stepwise
import org.scalatest.WordSpecLike
import scala.concurrent.Await
import akka.pattern.ask
import scala.concurrent.duration._
import scala.util.Success
import com.vngrs.ModelMarshallers._
import spray.json._
import DefaultJsonProtocol._

class PersisterActorEmptyDbSpec extends TestKit(ActorSystem("emptyDb")) with WordSpecLike with BeforeAndAfterAll {

  val dbFile = File.createTempFile("dbFileEmpty", "db")
  val dbFilePath = dbFile.getPath() 
  val ref = TestActorRef(new PersisterActor(dbFilePath))
  val persister = ref.underlyingActor

  implicit val timeOut: akka.util.Timeout = 5 seconds

  "Persister with an empty Database" should {
    "not contain any item from fresh start" in {
      assert(persister.indexSet.size === 0)
    }
    "add user correctly if email adress does not exists " in {
      val future = ref ? User("Cagdas", "asd", "cagdas@vngrs.com", "id", "itype")
      val Success(_) = future.value.get
      assert(persister.indexSet.size === 1)
    }
    "not add user if email address exists " in {
      val future = ref ? User("Ali", "asd", "cagdas@vngrs.com", "id", "itype")
      val Success(_) = future.value.get
      assert(persister.indexSet.size === 1)
    }

    "add a second user correctly if email adress does not exists " in {
      val future = ref ? User("Baris", "asd", "baris@vngrs.com", "id", "itype")
      val Success(_) = future.value.get
      assert(persister.indexSet.size === 2)
    }
  }

  override def afterAll() {
    dbFile.delete()
  }

}

class PersisterActorExistingDbSpec extends TestKit(ActorSystem("existig")) with WordSpecLike with BeforeAndAfterAll {

  val dbFile = File.createTempFile("dbFileEmpty", "db")
  val dbFilePath = dbFile.getPath() 

  val writer = new PrintWriter(new FileWriter(new File(dbFilePath), true))
  val u0 = User("Çağdaş", "asd", "cagdas@vngrs.com", "id", "itype")
  val u1 = User("Barış", "asd", "baris@vngrs.com", "id", "itype")
  writer.println(u0.toJson.compactPrint)
  writer.println(u1.toJson.compactPrint)
  writer.flush()
  writer.close()

  val ref = TestActorRef(new PersisterActor(dbFilePath))
  val persister = ref.underlyingActor
  implicit val timeOut: akka.util.Timeout = 5 seconds

  "Persister with an existing Database" should {
    "contain items from last execution" in {
      assert(persister.indexSet.size === 2)
    }
    "add user correctly if email adress does not exists " in {
      val future = ref ? User("Cagdas", "asd", "cagdas-New@vngrs.com", "id", "itype")
      val Success(_) = future.value.get
      assert(persister.indexSet.size === 3)
    }
    "not add user if email address exists from lastRun " in {
      val future = ref ? User("Ali", "asd", "cagdas@vngrs.com", "id", "itype")
      val Success(_) = future.value.get
      assert(persister.indexSet.size === 3)
    }

    "add a second user correctly if email adress does not exists " in {
      val future = ref ? User("Baris", "asd", "baris-New@vngrs.com", "id", "itype")
      val Success(_) = future.value.get
      assert(persister.indexSet.size === 4)
    }
  }
  override def afterAll() {
    dbFile.delete()
  }

}

