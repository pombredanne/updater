package com.vngrs

import spray.httpx.SprayJsonSupport
import spray.json.DefaultJsonProtocol
import spray.json.JsString
import spray.json.RootJsonWriter
import spray.json._
import DefaultJsonProtocol._

trait Validation {
  
  type Validator = User => ValidationResult

  val passValidator: Validator = u => if(u.password.length < 5) PassWordTooShort else Valid
  val userNameValidator: Validator = u => if(u.userName.length > 255) TooLongField else Valid
  
  val emailRegexp = """(?=[^\s]+)(?=(\w+)@([\w\.]+))""".r
  val mailValidator: Validator =  u => emailRegexp.findFirstIn(u.email) match {
    case None => MailValidationFailed
    case Some(_) => Valid
  }
  val validators: List[Validator] = List(passValidator, userNameValidator, mailValidator)

  def validate(u: User): ValidationResult = {
    validators.map( f=> f(u)).collect{ case x:InvalidResult => x} match {
      case Nil => Valid
      case x :: xs => InValid(x :: xs)
    }        
  }

}

trait ValidationResult
case object Valid extends ValidationResult
case class InValid(errors: List[InvalidResult]) extends ValidationResult

sealed trait InvalidResult extends ValidationResult
case object MailValidationFailed extends InvalidResult
case object TooLongField extends InvalidResult
case object PassWordTooShort extends InvalidResult
case object EmailExists extends InvalidResult

trait ValidationMarshallers extends DefaultJsonProtocol with SprayJsonSupport {
  implicit object InvalidResultJsonFormat extends RootJsonFormat[InvalidResult] {
    def write(v: InvalidResult) = v match {
      case MailValidationFailed => JsString("Please specify a valid email") 
      case TooLongField => JsString("Name field is too long. Use less then 256 chars") 
      case PassWordTooShort => JsString("Password is too short. It shoud have at least 5 characters") 
      case EmailExists => JsString("This email address is already in use ") 
    }
    
    //we don't need reader
    def read(x: JsValue) = ???
    
  }
  
  implicit val invalidFormat = jsonFormat1(InValid)
}

