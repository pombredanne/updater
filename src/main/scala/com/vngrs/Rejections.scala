package com.vngrs


import spray.routing.Rejection
import spray.routing.RejectionHandler
import spray.routing.Directives._
import spray.routing.MissingQueryParamRejection
import spray.routing.RejectionHandler
import spray.routing.Directives._
import spray.json.DefaultJsonProtocol
import spray.httpx.marshalling._
import spray.httpx.SprayJsonSupport._
import spray.http.StatusCodes._


trait Rejections extends ValidationMarshallers {

  val rejectionHandlers: RejectionHandler = RejectionHandler {
    case list if list.exists{authFailer _} => complete(Unauthorized , "Authentication Error")
    case ValidationError(invalid) :: _ =>
      complete(BadRequest, invalid)
    case MissingQueryParamRejection("apiKey") :: _ => complete(BadRequest, "API Key Required")
  }

  def authFailer(n: Rejection) = n match {
    case AuthenticationRejection => true
    case _ => false
  }

}


case object AuthenticationRejection extends Rejection
case class ValidationError(invalid: InValid) extends Rejection








