package com.vngrs

import java.io.FileInputStream
import java.util.Properties

trait ConfigComponent {
  val conf: Config
  trait Config {

    val apiKey: String
    val dbFile: String

  }
}

trait DefaultConfig extends ConfigComponent {


  
  val prop: Properties  = new Properties()
  prop.load(getClass.getResourceAsStream("/project.properties"))
  def getString(name: String): Option[String] = Option(prop.getProperty(name)) 

  
  override val conf = new Config {
    val apiKey: String = getString("apiKey").getOrElse("apiKey")
    val dbFile: String = getString("dbFile").getOrElse("/tmp/dbFile")
    val appPort: Int = getString("appPort").getOrElse("8080").toInt
  }

}
